part of couclient;

class GpsIndicator {
	Element containerE = querySelector("#gps-container");
	Element closeButtonE = querySelector("#gps-cancel");
	Element nextStreetE = querySelector("#gps-next");
	Element destinationE = querySelector("#gps-destination");
	Element arrowE = querySelector("#gps-direction");
	bool loadingNew,
		arriveFade = false;

	// Cache because they are relatively expensive to calculate
	// and will only change when the street changes.
	num nextSignX, nextSignY;
	bool onPath, arrived;

	GpsIndicator() {
		closeButtonE.onClick.listen((_) => this.cancel());

		new Service(['streetLoaded'], (_) {
			nextSignX = nextSignY = null;
			onPath = GPS.onPath;
			arrived = GPS.arrived;

			if (GPS.active && !onPath) {
				GPS.getRoute(currentStreet.label, GPS.destinationName);
			}
		});
	}

	void update() {
		if (loadingNew) {
			containerE.hidden = true;
			return;
		}

		if (!GPS.active) {
			cancel();
			return;
		}

		containerE.hidden = false;
		nextStreetE.text = GPS.nextStreetName;
		destinationE.text = GPS.destinationName;

		if (arrived) {
			arrowE.querySelector(".fa").classes
				..remove("fa-arrow-up")
				..add("fa-check");
			arrowE.style.transform = "rotate(0rad)";

			if (arriveFade || !currentStreet.loaded) {
				return;
			}

			arriveFade = true;
			new Timer(new Duration(seconds: 3), () {
				arriveFade = false;
				cancel();
			});
		} else {
			arrowE.style.transform = "rotate(${calculateArrowDirection()}rad)";
		}
	}

	void cancel() {
		if (GPS.active) {
			GPS.currentRoute.clear();
			GPS.active = false;
			transmit('gpsCancel', null);
			containerE.hidden = true;
			nextStreetE.text = destinationE.text = null;
			arrowE.style.transform = "";
			localStorage.remove("gps_navigating");
			new Timer(new Duration(seconds: 5), () {
				arrowE.querySelector(".fa").classes
					..add("fa-arrow-up")
					..remove("fa-check");
			});
		}
	}

	bool findNextSignpost() {
		for (Map<String, dynamic> exit in minimap.currentStreetExits) {
			if (exit["streets"].contains(GPS.nextStreetName)) {
				nextSignX = exit["x"];
				nextSignY = exit["y"] + 115; // center of the height

				if (exit["streets"].length > 1) {
					// signs on both sides
					nextSignX += 100;
				}
				break;
				// no point in wasting time, we found the signpost
			}
		}

		return nextSignX != null && nextSignY != null;
	}

	num calculateArrowDirection() {
		num playerX = CurrentPlayer.left;
		num playerY = CurrentPlayer.top + CurrentPlayer.height / 2;

		if ((nextSignX == null || nextSignY == null) && !findNextSignpost()) {
			// Error
			return 0;
		}

		num dx = nextSignX - playerX;
		num dy = nextSignY - playerY;
		num theta = atan2(dy, dx) - 3 * pi / 2;
		return theta;
	}
}
